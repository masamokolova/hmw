function Allfilms() {
  fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((response) => response.json())
    .then((arr) => {
      const list = document.querySelector("body");

      arr.forEach(({ episodeId, name, openingCrawl, characters }) => {
        const chlist = document.createElement("div");

        chlist.insertAdjacentHTML(
          "beforeend",
          `<h2> Эпизод : ${episodeId}</h2>
         <h2> Название : ${name}</h2>
         <h2> Краткое описание :</h2> <br> <div> ${openingCrawl}</div>`
        );

        list.insertAdjacentElement("beforeend", chlist);
        Allcharacters(characters, chlist);
      });
    });
}
function Allcharacters(charactersArray, chlist) {
  const charactersList = document.createElement("p");
  charactersList.insertAdjacentText("afterbegin", "Персонажи :");
  charactersArray.forEach((link) => {
    get(link).then(({ name }) => {
      charactersList.insertAdjacentHTML("beforeend", `<li>${name}</li>`);
    });
    chlist.insertAdjacentElement("beforeend", charactersList);
  });
}
Allfilms();
function get(link, query) {
  const request = new XMLHttpRequest();
  return fetch(`${link}${!!query ? +request : ""}`).then((response) =>
    response.json()
  );
}


