let color = ["white", "blue"];

document.getElementById("button").onclick = function () {
  let body = document.getElementById("body");

  if (body.style.background !== color[0]) {
    body.style.background = color[0];
    localStorage.setItem("color", color[0]);
  } else {
    body.style.background = color[1];
    localStorage.setItem("color", color[1]);
  }
};

if (localStorage.getItem("color")) {
  body.style.background = localStorage.getItem("color");
} else {
  localStorage.setItem("color", color[0]);
  body.style.background = color[0];
}
