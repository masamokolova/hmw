/*Событие – это сигнал от браузера о том, что что-то произошло. DOM-элементы принимают такие сигналы.
Бывают события мишы, клавиатуры, документа. 
К событию можно привязать обработчик событий (функцию), которая будет выполняться когда событие произошло.
С помощью обработчика событий, код реагирует на определенные действия пользователя.
*/

input.addEventListener("focus", function () {
  input.classList.add("input-frame");
  input.classList.add("input-letters-true");
  input.classList.remove("input-letters-false");
});

input.addEventListener("blur", function () {
  input.classList.remove("input-frame");
  let str = document.createElement("span");
  let val = document.getElementById("input").value;
  str.innerText = `Текущая цена: ${val}`;
  document.body.append(str);
  str.classList.add("but");
  let btn = document.createElement("button");
  btn.innerText = "X";
  btn.classList.add("but");
  document.body.append(btn);
  if (input.value <= 0) {
    input.style.border = "1px solid red";
    document.body.append(str);
    str.innerHTML = "Please enter correct price";
    input.classList.add("input-letters-false");
  }

  document.body.append(btn);
  btn.addEventListener("click", function () {
    document.getElementById("input").value = "";
    document.body.append(str);
    str.innerHTML = "";
    btn.remove();
  });
});
