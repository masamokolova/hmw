let button = document.getElementById("btn");
let textInformation = document.getElementById("text-information");

 async function request() {
    return (fetch("https://api.ipify.org/?format=json")
    .then((response) => response.json()))
  }
  
 async function personInformation(ip) {
    return ( fetch("http://ip-api.com/json/" + ip + "?fields=status,message,continent,country,regionName,city")
      .then((response) => response.json()));
  }

async function print() {
  const {ip} =  await request();
  const information = await personInformation(ip);

  if ((status = "success" || status === 200)) {
    const { continent, country, regionName, city } = information;

    textInformation.insertAdjacentHTML(
      "afterend",` 
      <div class="frame">
      <h3>Information about the person : </h3>
          <p><span class="text-color">Continent:</span> ${continent}</p>
          <p><span class="text-color">Country:</span> ${country}</p>
          <p><span class="text-color">Region:</span> ${regionName}</p>
          <p><span class="text-color">City:</span> ${city}</p>
          </div>`
    );
  }
}

button.addEventListener("click", print);