const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

function makeFormatedString(author, name, price) {
  return `${author}: "${name}", ${price}`;
}

function printText(text) {
  let ul = document.getElementById("root");
  ul.insertAdjacentHTML("beforeend", "<li>" + text + "</li>");
}

for (let fullText of books.map(function (e) {
  return makeFormatedString(e.author, e.name, e.price);
})) {
  printText(fullText);
  // console.log(fullText);
}

function renderBook(books) {
  books.forEach((element) => {
    const [validationResult, emptyFields] = validateFields(element);
    try {
      if (!validationResult) {
        throw new Error(`now fields : ${emptyFields.join(", ")}`);
      } else {
        createElem(element);
      }
    } catch (err) {
      console.log(err);
    }
  });
}
function validateFields(book) {
  const necessaryFields = ["author", "name", "price"];
  const bookFields = Object.keys(book);
  let validateResult = true;
  let emptyFields = [];

  if (necessaryFields.length !== bookFields.length) {
    validateResult = false;
  }
  necessaryFields.forEach((necessaryField) => {
    const isFieldsInBook = bookFields.includes(necessaryField);
    if (!isFieldsInBook) {
      emptyFields.push(necessaryField);
      validateResult = false;
    }
  });
  // console.log(emptyFields);
  return [validateResult, emptyFields];
}
function createElem(book) {
  // console.log(book);
}
renderBook(books);
