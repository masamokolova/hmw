/* С помощью деструктуризации мы можем разбить объект или массив на простые части,
 с которыми можем потом работать отдельно.
 Также можно присвоить объект или массив переменной (или даже нескольким переменным),
 предварительно разбив его на части.
 */

// ЗАДАНИЕ № 1

// let clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
// let clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

// let array = [...clients1, ...clients2]

// let valuesArr = array,
//     removeValFromIndex = [7,9];

// for (let i = removeValFromIndex.length -1; i >= 0; i--)
//    valuesArr.splice(removeValFromIndex[i],1);
//    console.log(valuesArr);

//      ЗАДАНИЕ № 2

// const charactersShortInfo = ([{ status, gender, ...rest }]) => rest;

// const characters = [
//   {
//     name: "Елена",
//     lastName: "Гилберт",
//     age: 17,
//     gender: "woman",
//     status: "human",
//   },
//   {
//     name: "Кэролайн",
//     lastName: "Форбс",
//     age: 17,
//     gender: "woman",
//     status: "human",
//   },
//   {
//     name: "Аларик",
//     lastName: "Зальцман",
//     age: 31,
//     gender: "man",
//     status: "human",
//   },
//   {
//     name: "Дэймон",
//     lastName: "Сальваторе",
//     age: 156,
//     gender: "man",
//     status: "vampire",
//   },
//   {
//     name: "Ребекка",
//     lastName: "Майклсон",
//     age: 1089,
//     gender: "woman",
//     status: "vempire",
//   },
//   {
//     name: "Клаус",
//     lastName: "Майклсон",
//     age: 1093,
//     gender: "man",
//     status: "vampire",
//   },
// ];

// console.log(charactersShortInfo(characters));

//      ЗАДАНИЕ № 3

// const user1 = {
//   name: "John",
//   years: 30,
// };

// let { name, years: age, isAdmin = false } = user1;

// console.log(name);
// console.log(age);
// console.log(isAdmin);

//     ЗАДАНИЕ № 4

// const satoshi2020 = {
//     name: 'Nick',
//     surname: 'Sabo',
//     age: 51,
//     country: 'Japan',
//     birth: '1979-08-21',
//     location: {
//       lat: 38.869422,
//       lng: 139.876632
//     }
//   }

//   const satoshi2019 = {
//     name: 'Dorian',
//     surname: 'Nakamoto',
//     age: 44,
//     hidden: true,
//     country: 'USA',
//     wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//     browser: 'Chrome'
//   }

//   const satoshi2018 = {
//     name: 'Satoshi',
//     surname: 'Nakamoto',
//     technology: 'Bitcoin',
//     country: 'Japan',
//     browser: 'Tor',
//     birth: '1975-04-05'
//   }

//   let fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020}
//   console.log(fullProfile);

//       ЗАДАНИЕ № 5

// const books = [{
//     name: 'Harry Potter',
//     author: 'J.K. Rowling'
//   }, {
//     name: 'Lord of the rings',
//     author: 'J.R.R. Tolkien'
//   }, {
//     name: 'The witcher',
//     author: 'Andrzej Sapkowski'
//   }];

//   const bookToAdd = {
//     name: 'Game of thrones',
//     author: 'George R. R. Martin'
//   }

// let book = [ ...books, bookToAdd]
// console.log(book);

//     ЗАДАНИЕ № 6

// const employee = {
//     name: 'Vitalii',
//     surname: 'Klichko'
//   }

//   const person = {...employee, age: 50, salary:35000}
//  console.log(person);

//      ЗАДАНИЕ № 7

// const array = ['value', () => 'showValue']
// let value = array[0]
// let showValue = array[1] = 'showValue';

// alert(value)
// alert(showValue);
