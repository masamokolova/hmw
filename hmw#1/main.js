/* Прототипное наследование помогает нам создать объект на основе другого объекта (не копируя его методы,свойства,а перейнять).
Если мы хотим посмотреть свойство из объекта(object),а его нету,то оно автоматически возьмет его с прототипа.
*/
class Employee {
  constructor(name, surname, salary) {
    this._name = name;
    this._surname = surname;
    this._salary = salary;
  }

  get name() {
    return `${this._name} `;
  }

  get surname() {
    return `${this._surname}`;
  }

  get salary() {
    return ` ${this._salary}`;
  }

  set name(name) {
    this._name = name;
  }

  set surname(surname) {
    this._surname = surname;
  }

  set salary(salary) {
    this._salary = salary;
  }

  walk() {
    console.log(this._name + this._salary + this._surname);
  }
}

let user = new Employee("Ivan", "Petrov", 3200);
console.log(user.name, user.surname, user.salary);

class Programmer extends Employee {
  constructor(name, surname, salary, lang) {
    super(name, surname, salary);
    this._lang = lang;

    this.getSalary = function () {
      return ` ${this._salary * 3}`;
    };
  }
  getLang() {
    return `${this._name} ${
      this._surname
    }${this.getSalary()}, list of languages : ${this._lang}`;
  }
}

let programmer = new Programmer(
  "Ivan",
  "Petrov",
  3200,
  "English,Ukrainian,Russian"
);
console.log(programmer.getLang());

let programmer1 = new Programmer(
  "Alina",
  "Makarevich",
  5490,
  "Russian,Chinese"
);
console.log(programmer1.getLang());

let programmer2 = new Programmer("Anton", "Solovey", 4530, "English,Ukrainian");
console.log(programmer2.getLang());

let programmer3 = new Programmer("Anna", "Vovk", 2500, "English");
console.log(programmer3.getLang());
