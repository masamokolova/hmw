window.addEventListener("DOMContentLoaded", function () {
  "use strict";
});

let tab = document.querySelector(".tabs");
let tabsContent = document.querySelectorAll(".tabs-content li");

tab.addEventListener("click", function (event) {
  [...tab.children].forEach((item) => {
    item.classList.remove("active");
    event.target.classList.add("active");
  });
  const index = [...tab.children].findIndex((elem) =>
    elem.classList.contains("active")
  );
  showText(index);
});


function showText(index) {
  tabsContent.forEach((item) => {
    item.setAttribute("hidden", true);
  });
  tabsContent[index].removeAttribute("hidden");
}
