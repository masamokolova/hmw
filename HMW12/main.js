/* 1.setTimeout позволяет вызвать функцию один раз 
setInterval позволяет вызывать функцию регулярно
2.Функция будет вызываться очень быстро, но планировщик будет вызывать функцию только после завершения выполнения кода.
3.Потому что цикл будет выполняться, а раз нам он уже не нужен, то нужно вызвать clearInterval() (он останавливает выполение функции).
*/

let  ImageList = new Array();

ImageList.addImage = function (ImageSrc) {
    let Img = new Image();
    Img.src = ImageSrc;
    this[ this.length ] = Img;
}

ImageList.addImage('img/1.jpg');
ImageList.addImage('img/2.jpg');
ImageList.addImage('img/3.jpg');
ImageList.addImage('img/4.png');

let imag = document.getElementsByTagName("img");
imag[0].classList.add("image-to-show");

ImageList.playing   = false;
ImageList.timer     = null;
ImageList.img       = null;
ImageList.imgFilter = null;
ImageList.cur       = 0;
ImageList.delay     = 3000; 


ImageList.play = function() {
    if ( !this.playing )
    {
        this.playing = true;
        this.next();
    }
}

ImageList.stop = function() {
    if ( this.playing )
    {
        clearTimeout(this.timer);
        this.timer = null;
        this.playing = false;
    }
}

ImageList.next = function() {
    if ( this.playing && this.length && this.img )
    {
        if ( this.cur >= this.length ) this.cur = 0;
        
        if ( !this[this.cur].complete )
        {
            setTimeout("ImageList.next()", 50); 
            return false;
        }
        
     
        

        this.img.src = this[ this.cur++ ].src;
        
        
        this.timer = setTimeout("ImageList.next()", this.delay);
        

        if ( this.imgFilter ) this.imgFilter.play();
        
        return true;
    }
}

ImageList.setImg = function(Img) {
    this.img = Img;
    this.imgFilter = null;
    
    try
    {
  
        for (var i in Img.filters)
        {
            if ( typeof(Img.filters[i]) != 'object' ) continue;
            if ( typeof(Img.filters[i].apply) == 'undefined' ) continue;
            if ( typeof(Img.filters[i].play ) == 'undefined' ) continue;
            
            this.imgFilter = Img.filters[i];
            break;
        }
    }
    catch(e)
    {
        
    }
}

ImageList.setImg(imgMain); 
ImageList.play();